package model;

public enum FieldTypes {
    F_LIFES(1, 999), // lake of specs
    F_KEYS(1, 5),
    F_BOMBS(1, 999); // lake of specs

    public int min;
    public int max;

    /**
    used to link inputs check values to their corresponding properties in classes (Player and Game)
    */
    FieldTypes(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public int getMin() {
        return min;
    }
    public int getMax() {
        return max;
    }
}
