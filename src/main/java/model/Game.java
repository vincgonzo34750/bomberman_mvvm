package model;

import javafx.animation.KeyFrame;
import javafx.beans.property.*;
import javafx.util.Duration;
import javafx.animation.Timeline;

import static model.CellValue.*;

public class Game {

    /**
     * Here start the Inner class PlayerMovement which is responsible for the movement of the player
     */
    class PlayerMovement {
        Position startingPosition;
        Position destinationPosition;
        Movement movement;

         PlayerMovement(Position playerPosition, Movement movement){
            this.startingPosition = playerPosition;
            this.movement = movement;
            this.destinationPosition = createDest(this.movement);
        }

        boolean checkMoveAllowed() {
            if (destinationPosition.isValid()){
                CellValue cv = Game.this.getCellValueProperty(destinationPosition.getX(), destinationPosition.getY()).getValue();
                if (cv != CellValue.WALL && cv != CellValue.PILLAR)
                    return true;
            }
            return false;
        }

        /**
         *
         * @param movement (direction in which the player want to go)
         * @return the new Position for the player, null otherwise
         */
        private Position createDest(Movement movement){
             Position dest = null;
            switch (movement) {
                case UP:
                    dest = new Position((startingPosition.getX()-1), startingPosition.getY());
                    break;
                case DOWN:
                    dest = new Position((startingPosition.getX()+1), startingPosition.getY());
                    break;
                case LEFT:
                    dest = new Position(startingPosition.getX(), (startingPosition.getY()-1));
                    break;
                case RIGHT:
                    dest = new Position(startingPosition.getX(), (startingPosition.getY()+1));
                    break;
            }
            return dest;
        }

        /**
         * if the player can move in the indicated direction, remove him from his cell and add him to the targeted cell
         */
        void setPlayerPosition(){
            if (checkMoveAllowed()){
                Cell start = Game.this.getCell(startingPosition);
                Cell dest = Game.this.getCell(destinationPosition);
                start.removeElement(getGameFacade().getPlayer());
                if (getCellValueProperty(start.getPosition().getX(),start.getPosition().getY()).getValue() == MINE)
                    activateMine(start.getPosition());
                Game.this.gameFacade.getPlayer().move(destinationPosition);

                switchCellValue(dest);

                dest.addElement(getGameFacade().getPlayer());
                if(Game.this.getCellWeGotWinnerValueProperty(destinationPosition).getValue())
                    Game.this.win();
            }
        }

        /**
         * Manages all elements which the Player can interact with on movement
         * @param dest (Cell in which the method verify the object)
         */
        void switchCellValue(Cell dest){
            CellValue cv = dest.getCellValueProperty().getValue();
            if (cv == CellValue.KEY){
                Game.this.decrementKey();
                dest.removeFirstElement();
            } else if (cv == CellValue.BONUSLIFE){
                Game.this.gameFacade.getPlayer().liveUp();
                dest.removeFirstElement();
            } else if (cv == CellValue.BONUSBOMB){
                Game.this.gameFacade.getPlayer().bombUp();
                dest.removeFirstElement();
            } else if (cv == MINE){
                dest.removeFirstElement();
            } else if (cv == CellValue.TREASURE){
                Treasure t = (Treasure) dest.removeFirstElement();
                do {
                    dest.addElement(t.removeComponent());
                    /**loop is managing composite pattern by checking for remaining components until empty*/
                } while (!t.getTreasureComps().isEmpty());
            }
        }

     } //end of inner class

    static final int DELAY = 1000;
    public static final Position STARTINGPOINT = new Position(0,0);
    public IntegerProperty keyAmount = new SimpleIntegerProperty();
    public BooleanProperty keyAmountIsCorrect = new SimpleBooleanProperty(false);

    private Grid grid;
    private GameFacade gameFacade;
    private GameStatus STATUS = GameStatus.GAME_NOT_LAUNCH;
    private final StringProperty gameStatusText = new SimpleStringProperty(STATUS.getStatus());
    private final ObjectProperty<GameStatus> gameStatus = new SimpleObjectProperty<>(STATUS);

    /**
     * Constructor of game class, launching game bindings
     * */
    public Game(GameFacade gameFacade){
        this.gameFacade = gameFacade;
        grid = new Grid();
        configBinding();
    }

    /**
     * Starts the game
     */
    public void start() {
        grid.reGenerateGrid(keyAmount.getValue());
        checkForMine();
        gameStatus.setValue(GameStatus.GAME_RUNNING);
    }


    public void instanciatePlayer(){
        gameFacade.getPlayer().positionProperty.setValue(STARTINGPOINT);
        getGrid().getCell(STARTINGPOINT).addElement(gameFacade.getPlayer());
    }

    /**
     * function to end the game
     */
    public void lose(){
        gameStatus.setValue(GameStatus.PLAYER_LOSE);
        gameFacade.removeListeners();
        resetValuesOnRestart();
    }

    /**
     * function to win the game
     */
    public void win(){
        gameStatus.setValue(GameStatus.PLAYER_WIN);
        gameFacade.removeListeners();
        resetValuesOnRestart();
    }

    /**
     * resetter is used to clear all input in the textfields of MenuViewModel
     *
     */
    public void resetValuesOnRestart(){
        gameFacade.resetter.setValue(true);
        gameFacade.getPlayer().resetBombs();
        gameFacade.getPlayer().resetLife();
        keyAmount.setValue(0);
        gameFacade.resetter.setValue(false);
    }

    /**
     * This is the main function triggered by Key pressed calling our inner Class PlayerMovement
     * @param movement
     */
    public void move(Movement movement){
        new PlayerMovement(gameFacade.getPlayerPositionProperty().getValue(), movement).setPlayerPosition();
    }

    /**
     * goes through the grid and check every cell for mines, and activate them if found
     */
    void checkForMine() {
        for (int i = 0; i < grid.GRID_WIDTH; ++i)
            for (int j = 0; j < grid.GRID_WIDTH; ++j)
                if(getCellValueProperty(i, j).getValue() == MINE)
                    activateMine(new Position(i, j));
    }


    /**
     * place a bomb at the player position and start a countdown before the bomb explosion
     */
    public void playerPlaceABomb(){
        Position target = gameFacade.getPlayerPositionProperty().getValue(); //Position au placement de la bombe
        Cell cell = grid.getCell(target);
        Bomb bomb = new Bomb();
        cell.addElement(bomb);
        gameFacade.getPlayer().useBomb();
        new Timeline(new KeyFrame(Duration.millis(DELAY), e -> {
            cell.removeElement(bomb);
            new Explosion(target, this);
        })).play();
    }

    /**
     * used in Explosion only for destruction matter
     * destruction nullcheck
     * @param position
     * @return the removed element, null otherwise
     */
    Element removeCellFirstElement(Position position){
        Element erm = this.getGrid().getCell(position).removeFirstElement();
        if (erm != null) {
            if (erm instanceof Player){
                System.out.println("Player died of own bomb");
                getGameFacade().killMTF();
                getGrid().getCell(Game.STARTINGPOINT).addElement(getGameFacade().getPlayer()); // ajoute le player à sa position de base initialisée
            }
            if (erm instanceof Key) {
                System.out.println("Player destroyed key");
                lose();
            }
            if (erm instanceof Wall){
                if (getCellValueProperty(position.getX(), position.getY()).getValue() == MINE){
                    activateMine(position);
                }
            }
        }
        return erm;
    }

    void activateMine(Position position){
        Cell c = grid.getCell(position);
        new Timeline(new KeyFrame(Duration.millis(DELAY*3), e -> {
            if (getCellValueProperty(position.getX(), position.getY()).getValue() == MINE){
                c.removeFirstElement();
                new Explosion(position, this);
            }
        })).play();
    }

    private void decrementKey() {
        keyAmount.setValue(keyAmount.getValue() - 1);
        if (keyAmount.getValue() == 0) win();
    }

    /**
     * digital check of inputs
     * @param str user's inputs
     * @param l integer to be checked
     * @return true if str is an integer, false otherwise
     */
    public boolean isInteger(String str, int l) {
            if(str == null || l == 0)
                return false;
            for(char c: str.toCharArray())
                if(!Character.isDigit(c))
                    return false;
            return true;
    }

    /**
     * Configures the bindings for the new game
     */
    private void configBinding(){
        gameStatus.addListener((obs, old, nw) -> {
            gameStatusText.setValue(nw.getStatus());
        });
        keyAmountIsCorrect.bind(keyAmount.greaterThanOrEqualTo(FieldTypes.F_KEYS.getMin())
                .and(keyAmount.lessThanOrEqualTo(FieldTypes.F_KEYS.getMax())));
    }

    ReadOnlyStringProperty getStatusTextValueProperty() {
        return gameStatusText; }

    Grid getGrid() {
        return grid;
    }

    private Cell getCell(Position pos){
        return grid.getCell(pos.getX(), pos.getY());
    }

    GameFacade getGameFacade() {
        return gameFacade;
    }

    IntegerProperty getKeyAmountValueProperty(){ return keyAmount; }

    /**
     * For external use
     * @return the status of the game
     */
    ReadOnlyObjectProperty<GameStatus> gameStatusProperty() { return gameStatus; }

    ReadOnlyBooleanProperty getKeyAmountIsCorrectValueProperty() { return keyAmountIsCorrect; }

    ReadOnlyObjectProperty<CellValue> getCellValueProperty(int line, int col){ return grid.getCellValueProperty(line, col); }

    ReadOnlyBooleanProperty getCellWeGotWinnerValueProperty(Position pos){ return grid.getWeGotWinnerValueProperty(pos.getX(), pos.getY()); }
}
