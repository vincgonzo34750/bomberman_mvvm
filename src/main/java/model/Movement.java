package model;

import javafx.scene.input.KeyCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum Movement {
    UP(Arrays.asList(KeyCode.E, KeyCode.NUMPAD8, KeyCode.UP, KeyCode.KP_UP)),
    DOWN(Arrays.asList(KeyCode.D, KeyCode.NUMPAD2, KeyCode.DOWN, KeyCode.KP_DOWN)),
    LEFT(Arrays.asList(KeyCode.S, KeyCode.NUMPAD4, KeyCode.LEFT, KeyCode.KP_LEFT)),
    RIGHT(Arrays.asList(KeyCode.F, KeyCode.NUMPAD6, KeyCode.RIGHT, KeyCode.KP_RIGHT)),
    BOMB(Arrays.asList(KeyCode.ENTER, KeyCode.SPACE)),
    NULL(Arrays.asList());

    List<KeyCode> list = new ArrayList<>();

    Movement(List<KeyCode> list){
        for(KeyCode k : list)
            this.list.add(k);
    }

    public static Movement getKeyCode(KeyCode k){
        for(Movement m : Movement.values())
            if(m.movementGetKeyCode(k))
                return m;
        return NULL;
    }

    /**
     * verify existing correspondance between possible movement and user entered keypress
     * @param code
     * @return
     */
    private boolean movementGetKeyCode(KeyCode code){
        boolean res = false;
        for(KeyCode k : this.list)
            if(k == code)
                res = true;
        return res;
    }
}
