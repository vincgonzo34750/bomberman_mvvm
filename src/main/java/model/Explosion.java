package model;

public class Explosion {
    Position position;
    Game game;
    private static final int RADIUS = 1;

    //used to instantiate an explosion to the target position
    public Explosion(Position position, Game game) {
        this.position = position;
        this.game = game;
        verticalCheck();
        horizontalCheck();
    }

    //explode every cell in a vertical line
    public void verticalCheck() {
        for (int i = -RADIUS; i <= RADIUS; ++i) {
            Position target = new Position(position.getX(), position.getY() + i);
            if (target.isValid()) {
                Element em = game.removeCellFirstElement(target);
                if (em != null) {
                    CellValue cv = em.getCv();
                    if (cv == CellValue.BONUSBOMB)
                        new Explosion(target, game); //trigger a chain explosion if the exploded cell contained a BombBonus
                }
            }
        }
    }

    //explode every cell horizontally except for the middle cell already exploded by the verticalCheck() method
    public void horizontalCheck() {
        for (int i = -RADIUS; i <= RADIUS; ++i) {
            Position target = new Position(position.getX() + i, position.getY());
            if (target.isValid() && i != 0) {
                Element em = game.removeCellFirstElement(target);
                if (em != null) {
                    CellValue cv = em.getCv();
                    if (cv == CellValue.BONUSBOMB)
                        new Explosion(target, game); //trigger a chain explosion if the exploded cell contained a BombBonus
                }
            }
        }
    }
}