package model;

import javafx.beans.property.*;

import java.util.*;

public class Cell {
    private Position position;
    private BooleanProperty hasPillar = new SimpleBooleanProperty(false);
    private BooleanProperty weGotWinner = new SimpleBooleanProperty(false);
    private ObjectProperty<CellValue> cellValueProperty = new SimpleObjectProperty(CellValue.EMPTY);
    private List<Element> elements = new ArrayList<>();

    Cell(Position pos){
        this.position = pos;
    }

    Cell(int x, int y){
        this(new Position(x, y));
    }

    Cell (Position pos, Element e){
        this(pos);
        addElement(e);
    }

    public void addElement(Element e){
        if(e instanceof Player){
            elements.add(0, e);
        } else {
            elements.add(e);
        }
        updateCellValueProperty();
    }

    /**
     *
     * @param e Element to be removed
     * @return true if removed
     */
    public boolean removeElement(Element e){
        boolean rm = elements.remove(e);
        updateCellValueProperty();
        return rm;
    }

    /**
     * Used after every insert/remove from elements to update display
     */
    private void updateCellValueProperty(){
        if (!elements.isEmpty()){
            cellValueProperty.setValue(elements.get(0).getCv());
        }else{
            cellValueProperty.setValue(CellValue.EMPTY);
        }
    }

    public Element removeFirstElement() {
        Element e = null;
        if (!elements.isEmpty() && !(elements.get(0) instanceof Pillar))
            e = elements.remove(0);
        updateCellValueProperty();
        return e;
    }

    Position getPosition() {
        return position;
    }

    ReadOnlyBooleanProperty getWeGotWinnerValueProperty() { return weGotWinner; }

    ReadOnlyObjectProperty<CellValue> getCellValueProperty(){ return cellValueProperty; }
}
