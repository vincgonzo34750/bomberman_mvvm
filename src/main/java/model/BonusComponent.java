package model;

public abstract class BonusComponent extends Element{

    public BonusComponent(CellValue cv) {
        super(cv);
    }

    boolean addComponent(BonusComponent bc){throw new UnsupportedOperationException();}

}
