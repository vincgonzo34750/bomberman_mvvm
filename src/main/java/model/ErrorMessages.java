package model;

public enum ErrorMessages {
    NO_ERROR(""),
    NOT_INT("Input value is not a Integer"),
    NOT_ENOUGH_BOMBS("Not Enough Bombs"),
    TOO_SMALL("The Integer is too small to be validated"),
    TOO_LONG("The Integer is too big to be validated"),
    EMPTY("The input field is Empty.");

    final String msg;

    ErrorMessages(String msg){
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
