package model;

import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyObjectProperty;

import java.util.*;

public class Grid {
    static final int INTEGER_CTRL = 2,
            GRID_WIDTH = 15;
    final double CALC_PROBA = 30; //represent the percent of wall on the grid
    final double BONUSLIFE_PROBA = 30; //represent the probability of a bonus to be a BonusLife
    final double BONUSBOMB_PROBA = 30; //represent the probability of a bonus to be a BonusBomb
    final double BONUSMINE_PROBA = 30; //represent the probability of a bonus to be a BonusMine
    final double TREASURE_PROBA = 40;
    final double BONUS_PROBA = 10;

    private Cell[][] matrix;
    private ArrayList<Position> wallsPositions = new ArrayList();
    private ArrayList<Position> bonusPositions = new ArrayList();
    private boolean containBonus = false;

    Grid() {
        createGrid();
        placeWalls();
    }

    /**
     * Creates the grid with empty cells and pillars only
     */
    private void createGrid() {
        this.matrix = new Cell[GRID_WIDTH][];
        for (int i = 0; i < GRID_WIDTH; ++i) {
            matrix[i] = new Cell[GRID_WIDTH];
            for (int j = 0; j < GRID_WIDTH; ++j) {
                if (i % INTEGER_CTRL == 1 && j % INTEGER_CTRL == 1) {
                    matrix[i][j] = new Cell(new Position(i, j), new Pillar());
                } else
                    matrix[i][j] = new Cell(i, j);
            }
        }
    }

    /**
     * Places the wall on created grid
     */
    private void placeWalls() {
        for (int i = 0; i < GRID_WIDTH; ++i) {
            for (int j = 0; j < GRID_WIDTH; ++j) {
                if (!(i == 0 && j < 3) && !(i < 3 && j == 0)) {
                    if (matrix[i][j].getCellValueProperty().getValue() != CellValue.PILLAR) {
                        if (Math.random() <= CALC_PROBA / 100) {
                            matrix[i][j].addElement(new Wall());
                            wallsPositions.add(new Position(i, j));
                        }
                    }
                }
            }
        }
    }

    /**
     * Places treasures on created grid
     */
    private void placeTreasures() {
        for (int i = 0; i < GRID_WIDTH; ++i) {
            for (int j = 0; j < GRID_WIDTH; ++j) {
                if (!(i == 0 && j < 3) && !(i < 3 && j == 0)) {
                    if (matrix[i][j].getCellValueProperty().getValue() != CellValue.PILLAR) {
                        if (Math.random() <= BONUS_PROBA / 100) {
                            Element e = randomizeElement();
                            matrix[i][j].addElement(e);
                            if (e instanceof Treasure) {
                                System.out.println("Treasure placed at pos : (" + i + ", " + j + ")");
                                System.out.println("-------------------------------------");
                            }
                            if (containBonus){
                                containBonus = false;
                                bonusPositions.add(new Position(i, j));
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * BONUSCOMPONENT IS AN EXTENSION OF ELEMENT AND CAN BE RETURNED AS SUCH
     */
    private BonusComponent createTreasure(Treasure tr){
        System.out.println("Treasure content : ");
        for(int i = 0; i<3 ; i++)
            if(Math.random() <= TREASURE_PROBA / 100 || i == 0)
                tr.addComponent(randomizeElement());
        for (BonusComponent trc : tr.getTreasureComps())
            System.out.println(trc.toString());
        return tr;
    }


    /**
     * generate a bonus based on bonuses proba
     * @return created bonus
     */
    private BonusComponent randomizeElement(){
        double random = Math.random();
        if(random*100 <= BONUSLIFE_PROBA){
            containBonus = true;
            return new BonusLife();
        }else if(random*100 <= BONUSLIFE_PROBA + BONUSBOMB_PROBA){
            containBonus = true;
            return new BonusBomb();
        }else if(random*100 <= BONUSLIFE_PROBA + BONUSBOMB_PROBA + BONUSMINE_PROBA){
            return new Mine();
        }else{
            containBonus = true;
            return createTreasure(new Treasure());
        }
    }


    /**
     * replace walls exactly where they were previously
     */
    private void rePlaceWalls() {
        for (Position wallp : wallsPositions)
            matrix[wallp.getX()][wallp.getY()].addElement(new Wall());
    }

    /**
     * regenerate the grid for a new game
     * @param keys number of key to place
     */
    void reGenerateGrid(int keys) {
        bonusPositions = new ArrayList();
        createGrid();
        rePlaceWalls();
        placeTreasures();
        placeKeys(keys);
    }

    /**
     * place keys under walls or bonuses
     * @param amount of key to place
     */
    private void placeKeys(int amount) {
        Set<Position> set = new LinkedHashSet<>(bonusPositions);
        set.addAll(wallsPositions);
        bonusPositions = new ArrayList<>(set);
        List<Position> keyPlacements = new ArrayList<>();
        do{
            Random rand = new Random();
            Position p = bonusPositions.get(rand.nextInt(bonusPositions.size()));
            if (!keyPlacements.contains(p)){
                keyPlacements.add(p);
                --amount;
            }
            System.out.println("Key on {X:" + p.getY() + "-Y:" + p.getX() + "}");
        }while(amount > 0);

        for (Position pos: keyPlacements)
        {
            matrix[pos.getX()][pos.getY()].addElement(new Key());
        }
    }

    /**
     * used to verify if there is a visible key on the grid when the player has no bomb left
     * @return amount of visible keys
     */
    public int containKey() {
        int keyFound = 0;
        int i = 0;
        int j = 0;
        while (i < GRID_WIDTH) {
            while (j < GRID_WIDTH ) {
                if (getCellValueProperty(i, j).getValue() == CellValue.KEY)
                    keyFound++;
                j++;
            }
            i++;
            j = 0;
        }
        return keyFound;
    }


    public Cell getCell(int line, int col) {
        return matrix[line][col];
    }

    public Cell getCell(Position pos) {
        return matrix[pos.getX()][pos.getY()];
    }

    ReadOnlyObjectProperty<CellValue> getCellValueProperty(int line, int col) {
        return matrix[line][col].getCellValueProperty();
    }

    ReadOnlyBooleanProperty getWeGotWinnerValueProperty(int line, int col) {
        return matrix[line][col].getWeGotWinnerValueProperty();
    }
}
