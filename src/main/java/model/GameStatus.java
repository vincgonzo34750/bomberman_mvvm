package model;

public enum GameStatus {
    GAME_NOT_LAUNCH("Game not launched yet."),
    GAME_RUNNING("Game is running"),
    PLAYER_LOSE("Player lost..."),
    PLAYER_WIN("Player won !");

    public String status;

    GameStatus(String s) {
        this.status = s;
    }

    public String getStatus() {
        return status;
    }
}
