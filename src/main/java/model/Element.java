package model;

abstract class Element {
    private CellValue cv;

    public Element( CellValue cv) {
        this.cv = cv;
    }

    protected CellValue getCv() {
        return cv;
    }
}
