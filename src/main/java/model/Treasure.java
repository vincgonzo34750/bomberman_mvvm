package model;

import java.util.ArrayList;
import java.util.List;

public class Treasure extends BonusComponent{

    final int TREASURESIZE = 2;
    private List<BonusComponent> treasureContent = new ArrayList<>();

    Treasure() {
        super(CellValue.TREASURE);
    }

    @Override
    boolean addComponent(BonusComponent bc) {
        return sizeCheck() && treasureContent.add(bc);
    }

    BonusComponent removeComponent(){
        if (!treasureContent.isEmpty())
            return treasureContent.remove(0);
        return null;
    }

    public boolean sizeCheck(){
        return treasureContent.size() <= TREASURESIZE;
    }

    public List<BonusComponent> getTreasureComps(){
        return treasureContent;
    }
}
