package model;

public enum CellValue {
    PLAYER, BOMB, KEY, WALL, PILLAR, BONUSBOMB, BONUSLIFE, MINE, TREASURE, EMPTY;
}
