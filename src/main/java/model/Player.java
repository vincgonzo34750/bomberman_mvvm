package model;

import javafx.beans.property.*;

import static model.FieldTypes.*;
import static model.Game.STARTINGPOINT;

public class Player extends Element {

    private final BooleanProperty deadProperty = new SimpleBooleanProperty(false);
    private final BooleanProperty noBombsProperty = new SimpleBooleanProperty(false);
    private final BooleanProperty lifePropertyIsCorrect = new SimpleBooleanProperty(false);
    private final BooleanProperty bombsPropertyIsCorrect = new SimpleBooleanProperty(false);

    private IntegerProperty lifeProperty = new SimpleIntegerProperty();
    private IntegerProperty bombsProperty = new SimpleIntegerProperty();
    public SimpleObjectProperty<Position> positionProperty = new SimpleObjectProperty();

    public Player(){
        super(CellValue.PLAYER);
        configBindings();
    }

    private void configBindings(){
        deadProperty.bind(lifeProperty.greaterThanOrEqualTo(F_LIFES.getMin()));
        noBombsProperty.bind(bombsProperty.greaterThan(0));
        lifePropertyIsCorrect.bind(lifeProperty.greaterThanOrEqualTo(F_LIFES.getMin()).and(lifeProperty.lessThanOrEqualTo(F_LIFES.getMax())));
        bombsPropertyIsCorrect.bind(bombsProperty.greaterThanOrEqualTo(F_BOMBS.getMin()).and(bombsProperty.lessThanOrEqualTo(F_BOMBS.getMax())));
    }

    public void liveDown() {
        if (playerLive()){
            lifeProperty.setValue(lifeProperty.get() - 1);
            move(STARTINGPOINT);
        }
    }

    public void liveUp() {
        if (playerLive()){
            lifeProperty.setValue(lifeProperty.get() + 1);
        }
    }

    public void useBomb() {
        if (playerHaveBombs()) {
            bombsProperty.setValue(bombsProperty.get() - 1);
        }
    }

    public void bombUp() {
        bombsProperty.setValue(bombsProperty.get() + 1);
    }

    public void setFieldPlayerValue(int values, FieldTypes input) {
        if (input == F_LIFES) {
            lifeProperty.setValue(values);
        }
        if (input == FieldTypes.F_BOMBS) {
            bombsProperty.setValue(values);
        }
    }

    public void move(Position position) {
        positionProperty.setValue(position);
    }

    public void resetLife(){
        lifeProperty.setValue(0);
    }

    public void resetBombs(){
        bombsProperty.setValue(0);
    }

    public boolean playerLive(){ return lifeProperty.get() > 0; }

    public boolean playerHaveBombs(){
        return bombsProperty.get() > 0;
    }

    public ReadOnlyBooleanProperty getDeadProperty() { return deadProperty; }

    public ReadOnlyBooleanProperty getNoBombsAnymoreProperty() { return noBombsProperty; }

    public ReadOnlyBooleanProperty getBombIsCorrectValueProperty() { return bombsPropertyIsCorrect; }

    public ReadOnlyBooleanProperty getLifeIsCorrectValueProperty() { return lifePropertyIsCorrect; }

    public ReadOnlyIntegerProperty getBombsValueProperty() { return bombsProperty; }

    public ReadOnlyIntegerProperty getLifeValueProperty() { return lifeProperty; }

    public ReadOnlyObjectProperty<Position> getPlayerPositionProperty() { return positionProperty; }

}
