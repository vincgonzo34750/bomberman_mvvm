package model;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.util.Duration;


import static model.ErrorMessages.*;
import static model.FieldTypes.*;

public class GameFacade {
    private final Game game;
    private Player player;

    private final BooleanProperty isStartable = new SimpleBooleanProperty(false);
    private final BooleanProperty isStarted = new SimpleBooleanProperty(false);
    private final BooleanProperty playerDisplay = new SimpleBooleanProperty(false);
    public final BooleanProperty noErrorMessages = new SimpleBooleanProperty(false);
    private ChangeListener<? super Movement> listenerMovement;
    private ChangeListener<? super Boolean> listenerPlayerDead;
    private ChangeListener<? super Boolean> listenerNoBombs;

    public final BooleanProperty resetter = new SimpleBooleanProperty(false);

    static public SimpleObjectProperty<Movement> movementProperty = new SimpleObjectProperty();

    /**
     * Constructs the game facade and binds the properties for the GameStatus
     */
    public GameFacade() {
        this.game = new Game(this);
        this.player = new Player();
        //Checks if the game is Startable
        playerDisplay.bind(isStarted);
        playerDisplay.addListener((obs, old, nw)
                -> { if(nw) game.instanciatePlayer(); });
        //Checks if the game button is active
        isStartable.bind(getBombIsCorrectValueProperty()
                .and(getKeyAmountIsCorrectValueProperty()
                .and(getLifeIsCorrectValueProperty()))
                .and(isStarted.not())
                .and(noErrorMessages));

        isStarted.bind(game.gameStatusProperty().isEqualTo(GameStatus.GAME_RUNNING));
    }

    /**
     * Starts the game
     */
    public void start() {
        game.start();
        configBinding();
    }

    public void killMTF() {
        player.liveDown();
    } //kill My Tender Friend

    /**
     * Config binding for MVVM
     */
    private void configBinding(){
        listenerInitializer();
        getDeadValueProperty().addListener(listenerPlayerDead);
        getNoBombsAnymoreValueProperty().addListener(listenerNoBombs);
        movementProperty.addListener(listenerMovement);
    }

    public void listenerInitializer(){
        /** modify property Movement in Game to create logic of ask / refuse movement on context */
        listenerMovement = (obs, old, nw) -> {
            if(nw != Movement.NULL && nw != Movement.BOMB) game.move(nw);
            else if(nw == Movement.BOMB && player.getBombsValueProperty().getValue() > 0) {
                game.playerPlaceABomb();
            }
        };

        /** listener to watch boolean of living player to trigger game lose */
        listenerPlayerDead = (obs, old, nv) -> {
            game.lose();
        };

        /** If all the keys are not visible on board and the player has no bombs anymore, game is considered lost */
        listenerNoBombs = (obs, old, nw) ->{
            new Timeline(new KeyFrame(Duration.millis(game.DELAY + 100), e -> {
                if (game.getGrid().containKey() < getKeyAmountValueProperty().getValue() && getBombNumberProperty().getValue() <= 0)
                    //we re-verify if the player has zero bombs again at the end in case the player pick up a bomb bonus
                    game.lose();
            })).play();
        };
    }

    public void removeListeners() {
        getDeadValueProperty().removeListener(listenerPlayerDead);
        getNoBombsAnymoreValueProperty().removeListener(listenerNoBombs);
        movementProperty.removeListener(listenerMovement);
    }

    /**
     * Set values of input, if values are correct assigns values to player and game
     * @param value in input field
     * @param input concerned field (bomb, key or life) see FieldTypes enum
     * @return Error message to be returned if value is wrong (NO_ERROR if valid)
     */
    public ErrorMessages setFieldValue(String value, FieldTypes input){
        if (value == null || value.isEmpty())
            return EMPTY;
        int l = value.length();
        if(l > 3)
            return TOO_LONG;
        if(game.isInteger(value, l)){
            int parsedValue = Integer.parseInt(value);
            if(input == F_LIFES)
                if(parsedValue < input.getMin())
                    return TOO_SMALL;
                else if(parsedValue > input.getMax())
                    return TOO_LONG;
                else{
                    getPlayer().setFieldPlayerValue(Integer.parseInt(value), input);
                    return NO_ERROR;
                }
            else if(input == F_KEYS)
                if(parsedValue < input.getMin())
                    return TOO_SMALL;
                else if(parsedValue > input.getMax())
                    return TOO_LONG;
                else{
                    game.getKeyAmountValueProperty().setValue(Integer.parseInt(value));
                    return NO_ERROR;
                }
            else if(input == F_BOMBS) // F_BOMBS FieldTypes
                if(getKeyAmountValueProperty().getValue() != null && parsedValue < getKeyAmountValueProperty().getValue())
                    return NOT_ENOUGH_BOMBS;
                else if(parsedValue > input.getMax())
                    return TOO_LONG;
                getPlayer().setFieldPlayerValue(Integer.parseInt(value), input);
                return NO_ERROR;
        }
        return NOT_INT;
    }

    /**
     * Static Getter
     * @return Grid width
     */
    public static int gridWidth() { return Grid.GRID_WIDTH; }

    public Player getPlayer(){ return player; }

    /**
     * Checks if game is started property
     * @return true if the game is started
     */
    public ReadOnlyBooleanProperty isStartedValueProperty() {
        return isStarted;
    }

    /**
     * Checks if the game is in stop or not running & curernt game state
     * @return true if the game is in game status before game running
     */
    public ReadOnlyBooleanProperty isStartableValueProperty() { return isStartable; }

    /**
     *
     * @return String
     */
    public ReadOnlyStringProperty getStatusValueProperty(){ return game.getStatusTextValueProperty(); }

    /** @return true if counter bomb <= 0 */
    public ReadOnlyBooleanProperty getNoBombsAnymoreValueProperty(){ return player.getNoBombsAnymoreProperty(); }

    /** @return true if counter life <= 0 */
    public ReadOnlyBooleanProperty getDeadValueProperty(){ return player.getDeadProperty(); }

    /** number of bombs in Player obj
     * @return int  */
    public ReadOnlyIntegerProperty getBombsValueProperty() { return player.getBombsValueProperty(); }

    /** number of lives in Player obj
     * @return int  */
    public ReadOnlyIntegerProperty getLifeValueProperty() { return player.getLifeValueProperty(); }

    /**
     * Checks the game status property
     * @return the game status
     */
    public ReadOnlyObjectProperty<GameStatus> gameStatusProperty() {
        return game.gameStatusProperty();
    }

    /**
     * Cell Property
     * @param line
     * @param col
     * @return the value of the cell to be shown
     */
    public ReadOnlyObjectProperty<CellValue> getCellValueProperty(int line, int col){ return game.getCellValueProperty(line, col); }

    public ReadOnlyObjectProperty<Position> getPlayerPositionProperty() { return player.getPlayerPositionProperty(); }

    public ReadOnlyBooleanProperty getKeyAmountIsCorrectValueProperty() { return game.getKeyAmountIsCorrectValueProperty(); }

    public ReadOnlyBooleanProperty getBombIsCorrectValueProperty() { return player.getBombIsCorrectValueProperty(); }

    public ReadOnlyIntegerProperty getBombNumberProperty() { return player.getBombsValueProperty(); }

    public ReadOnlyBooleanProperty getLifeIsCorrectValueProperty() { return player.getLifeIsCorrectValueProperty(); }

    public ReadOnlyIntegerProperty getKeyAmountValueProperty() { return game.getKeyAmountValueProperty(); }
}
