package app;

import javafx.application.Application;
import javafx.stage.Stage;
import view.BomberView;

public class BomberApp extends Application  {

    @Override
    public void start(Stage primaryStage) throws Exception {
       new BomberView(primaryStage);
    }

    public static void main(String[] args) {
        launch();
    }

}
