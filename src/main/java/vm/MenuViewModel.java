package vm;

import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import model.ErrorMessages;
import model.FieldTypes;
import model.GameFacade;
import model.GameStatus;

public class MenuViewModel {
    private final String NBR_LIFE_TXT = "Number life : ",
            NBR_BOMBS_TXT = "Number bombs : ",
            STATUS_TXT = "Party status is : ",
            NBR_KEYS_TXT = "Number Keys will be : ";

    private final StringProperty gameStatusTextProperty = new SimpleStringProperty(STATUS_TXT + GameStatus.GAME_NOT_LAUNCH.getStatus());
    private final StringProperty gameBombTextProperty = new SimpleStringProperty();
    private final StringProperty gameKeysNbrTextProperty = new SimpleStringProperty();
    private final StringProperty gameLifeTextProperty = new SimpleStringProperty();

    private final StringProperty playerNbrLife = new SimpleStringProperty();
    private final StringProperty playerNbrBombs = new SimpleStringProperty();
    private final StringProperty gameNbrKeys = new SimpleStringProperty();
    private final StringProperty playerNbrLifeErrorText = new SimpleStringProperty();
    private final StringProperty playerNbrBombsErrorText = new SimpleStringProperty();
    private final StringProperty gameNbrKeysErrorText = new SimpleStringProperty();
    private final BooleanProperty allErrorMessagesText = new SimpleBooleanProperty(false);

    private final GameFacade gameFacade;

    public MenuViewModel(GameFacade gameFacade) {
        this.gameFacade = gameFacade;
        configNameHandlers();
        configLogicStatus();
        configLabels();
    }

    private void configNameHandlers() {
        gameFacade.gameStatusProperty().addListener((obs, old, nw) ->
            gameStatusTextProperty.setValue(STATUS_TXT + nw.getStatus()));
        gameFacade.getBombsValueProperty().addListener((obs, old, nw) ->
            gameBombTextProperty.setValue(NBR_BOMBS_TXT + nw));
        gameFacade.getLifeValueProperty().addListener((obs, old, nw) ->
                gameLifeTextProperty.setValue(NBR_LIFE_TXT + nw));

        gameFacade.resetter.addListener((obs, old,nw) ->
                clearAllInput());

        allErrorMessagesText.addListener((obs, old, nw) -> {
            gameFacade.noErrorMessages.setValue(nw);
        });
        gameFacade.getKeyAmountValueProperty().addListener((obs, old, nw) ->
                gameKeysNbrTextProperty.setValue(NBR_KEYS_TXT + nw));
    }

    private void configLogicStatus() {
        gameFacade.getDeadValueProperty().addListener((obs, old, nw) ->
                gameStatusTextProperty.setValue(STATUS_TXT + gameFacade.getStatusValueProperty().getValue()));
        gameFacade.getNoBombsAnymoreValueProperty().addListener((obs, old, nw) ->
                gameStatusTextProperty.setValue(STATUS_TXT + gameFacade.getStatusValueProperty().getValue()));
        /**
         * This Handler check @ the Values enter into MenuView Fields to generate error msg if input not accross the required values. */
        ChangeListener<String> namesHandler = (obs, oldval, newval) -> {
            gameNbrKeysErrorText.setValue(
                    errorMsgForField( gameFacade.setFieldValue(gameNbrKeys.getValue(), FieldTypes.F_KEYS)) );
            playerNbrLifeErrorText.setValue(
                    errorMsgForField( gameFacade.setFieldValue(playerNbrLife.getValue(), FieldTypes.F_LIFES)) );
            playerNbrBombsErrorText.setValue(
                    errorMsgForField( gameFacade.setFieldValue(playerNbrBombs.getValue(), FieldTypes.F_BOMBS)) );
            allErrorMessagesText.setValue(checkAllMessagesErrors());
        };
        playerNbrLife.addListener(namesHandler);
        playerNbrBombs.addListener(namesHandler);
        gameNbrKeys.addListener(namesHandler);
    }

    private static String errorMsgForField( ErrorMessages errorMsg ) {
       return errorMsg.getMsg();
    }

    private void configLabels(){
        gameBombTextProperty.setValue(NBR_BOMBS_TXT + gameFacade.getBombsValueProperty().get());
        gameLifeTextProperty.setValue(NBR_LIFE_TXT + gameFacade.getLifeValueProperty().get());
        gameKeysNbrTextProperty.setValue(NBR_KEYS_TXT + gameFacade.getKeyAmountValueProperty().get());
    }

    private void clearAllInput(){
        playerNbrLife.setValue("");
        playerNbrBombs.setValue("");
        gameNbrKeys.setValue("");
    }

    /**
     * This method verify that every error messages is empty ( equal to ErrorMessages.NO_ERROR )
     * true if all three messages equals "" else false
     * @return bool
     */
    private boolean checkAllMessagesErrors(){
        if( !gameNbrKeysErrorText.getValue().equals(ErrorMessages.NO_ERROR.getMsg())
            || !playerNbrBombsErrorText.getValue().equals(ErrorMessages.NO_ERROR.getMsg())
            || !playerNbrLifeErrorText.getValue().equals(ErrorMessages.NO_ERROR.getMsg())){
            return false;
        }
        return true;
    }

    public ReadOnlyBooleanProperty isGameStartedProperty() { return gameFacade.isStartedValueProperty(); }

    public StringProperty playerNbrLifeProperty() {
        return playerNbrLife;
    }

    public StringProperty playerNbrBombsProperty() { return playerNbrBombs; }

    public StringProperty gameNbrKeysProperty() { return gameNbrKeys; }

    public ReadOnlyStringProperty inputLifeErrorProperty() { return playerNbrLifeErrorText; }

    public ReadOnlyStringProperty inputBombsErrorProperty() { return playerNbrBombsErrorText; }

    public ReadOnlyStringProperty inputKeysAmountErrorProperty() { return gameNbrKeysErrorText; }

    public ReadOnlyStringProperty nbrKeysLabelProperty() { return gameKeysNbrTextProperty; }

    public ReadOnlyStringProperty gameStatusTextProperty() { return gameStatusTextProperty; }

    public ReadOnlyStringProperty nbrBombsTextProperty() { return gameBombTextProperty; }

    public ReadOnlyStringProperty nbrLifesTextProperty() { return gameLifeTextProperty; }
}
