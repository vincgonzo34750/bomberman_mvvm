package vm;

import javafx.beans.property.*;
import javafx.geometry.Pos;
import model.Cell;
import model.CellValue;
import model.GameFacade;
import model.Position;

public class CellViewModel {

    private final GameFacade game;
    private final int line, col;
    private IntegerProperty lineProperty = new SimpleIntegerProperty();
    private IntegerProperty columnProperty = new SimpleIntegerProperty();

    public CellViewModel(int line, int col, GameFacade game) {
        this.line = line;
        this.col = col;
        this.game = game;
        lineProperty.setValue(line);
        columnProperty.setValue(col);
    }

    /**
     * Cell Property
     * @return the value of the cell to be shown
     */
    public ReadOnlyObjectProperty<CellValue> getCellValueProperty(){ return game.getCellValueProperty(line, col); }

}
