package vm;

import javafx.beans.property.*;
import javafx.scene.input.KeyCode;
import model.GameFacade;
import model.Movement;

public class BomberViewModel {
    private final String APP_TITLE = "BomberGirl";
    private final MenuViewModel menuViewModel;
    private final GridViewModel gridViewModel;
    private final GameFacade gameFacade;

    public MenuViewModel getMenuViewModel() {
        return menuViewModel;
    }

    public GridViewModel getGridViewModel() {
        return gridViewModel;
    }

    public BomberViewModel(GameFacade gameFacade) {
        this.gameFacade = gameFacade;
        menuViewModel = new MenuViewModel(gameFacade);
        gridViewModel = new GridViewModel(gameFacade);
    }

    public void triggerStart(){
        gameFacade.start();
    }

    /**
     * @return SimpeStringProperty */
    public ReadOnlyStringProperty titleProperty() {
        return new SimpleStringProperty(APP_TITLE);
    }

    public ReadOnlyBooleanProperty isGameStartedProperty() { return gameFacade.isStartedValueProperty(); }

    public ReadOnlyBooleanProperty isGameStartableProperty() { return gameFacade.isStartableValueProperty(); }
}
