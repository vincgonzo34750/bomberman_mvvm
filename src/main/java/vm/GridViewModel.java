package vm;

import model.GameFacade;

public class GridViewModel {

    private final GameFacade game;

    public GridViewModel(GameFacade game) {
        this.game = game;
    }
    public CellViewModel getCellViewModel(int line, int col) {
        return new CellViewModel(line, col, game);
    }
}
