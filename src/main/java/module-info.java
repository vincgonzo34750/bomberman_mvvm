module eu.epfc.anc3.anc3_oxo {
    requires javafx.controls;
    requires junit;

    exports view;
    exports model;
    exports app;

}