package view;

import javafx.beans.property.DoubleProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import vm.MenuViewModel;

import static view.BomberView.*;

public class MenuView extends HBox {
    private final Label statusLabel = new Label();
    private final Label nbrLifes = new Label();
    private final Label nbrBombs = new Label();
    private final Label nbrKeysLabel = new Label();

    private final TextField playerLivesNumber = new TextField();
    private final TextField playerBombsNumber = new TextField();
    private final TextField gameKeysNumber = new TextField();


    private final Label inputLifeErrorLabel = new Label("");
    private final Label inputBombsErrorLabel = new Label("");
    private final Label inputKeyAmountErrorLabel = new Label("");

    private final MenuViewModel menuViewModel;

    public MenuView(MenuViewModel menuViewModel, DoubleProperty gridWidthProperty) {
        this.menuViewModel = menuViewModel;
        configMenu(gridWidthProperty);
    }

     void configMenu(DoubleProperty gridWidthProperty) {
        BorderPane relRoot = new BorderPane();
        relRoot.minWidthProperty().setValue(GRID_WIDTH * 2);
        relRoot.setStyle("-fx-background-color: white; -fx-grid-lines-visible: true;-fx-border-color: black;");
        GridPane root = new GridPane();
        root.setHgap(PADDING / 2);
        root.setVgap(PADDING / 2);
        root.setPadding(new Insets(PADDING));
        //setPadding(new Insets(PADDING));
        root.setPrefWidth(gridWidthProperty.getValue() * 2);
        /** Menu section Life */

        root.add(nbrLifes, 0, 0);
        root.add(playerLivesNumber, 1, 0);
        root.add(inputLifeErrorLabel, 2, 0);
         /** Menu section Bombs */
        root.add(nbrBombs, 0, 1);
        root.add(playerBombsNumber, 1, 1);
        root.add(inputBombsErrorLabel, 2, 1);
         /** Menu section Keys */
         root.add(nbrKeysLabel, 0, 3);
         root.add(gameKeysNumber, 1, 3);
         root.add(inputKeyAmountErrorLabel, 2, 3);
         /** Menu section GameStatus*/
         root.add(statusLabel, 0, 4);

        relRoot.setCenter(root);
        setPadding(new Insets(PADDING, PADDING, 0, PADDING));
        getChildren().add(relRoot);
        configLabels();

    }

    void configLabels() {
        statusLabel.textProperty().bind(menuViewModel.gameStatusTextProperty());
        nbrLifes.textProperty().bind(menuViewModel.nbrLifesTextProperty());
        nbrBombs.textProperty().bind(menuViewModel.nbrBombsTextProperty());
        nbrKeysLabel.textProperty().bind(menuViewModel.nbrKeysLabelProperty());

        inputLifeErrorLabel.textProperty().bind(menuViewModel.inputLifeErrorProperty());
        inputBombsErrorLabel.textProperty().bind(menuViewModel.inputBombsErrorProperty());
        inputKeyAmountErrorLabel.textProperty().bind(menuViewModel.inputKeysAmountErrorProperty());

        playerLivesNumber.textProperty().bindBidirectional(menuViewModel.playerNbrLifeProperty());
        playerBombsNumber.textProperty().bindBidirectional(menuViewModel.playerNbrBombsProperty());
        gameKeysNumber.textProperty().bindBidirectional(menuViewModel.gameNbrKeysProperty());
        playerLivesNumber.disableProperty().bind(menuViewModel.isGameStartedProperty());
        playerBombsNumber.disableProperty().bind(menuViewModel.isGameStartedProperty());
        gameKeysNumber.disableProperty().bind(menuViewModel.isGameStartedProperty());
    }
}
