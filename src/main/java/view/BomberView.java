package view;

import javafx.beans.property.*;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.GameFacade;
import model.Movement;
import vm.BomberViewModel;

public class BomberView extends BorderPane {
    // Constants of the Game Scene
    static final int PADDING = 10;
    private static final int SCENE_MIN_WIDTH = 600;
    private static final int SCENE_MIN_HEIGHT = 800;
    static final int GRID_WIDTH = GameFacade.gridWidth();
    private final String BTN_PLAY = "Play";
    private final BorderPane btnContainer = new BorderPane();
    private Scene scene;


    private final Button startButton = new Button();

    private final BomberViewModel bomberViewModel = new BomberViewModel(new GameFacade());
    private GridView gridView;
    private MenuView menuView;

    private final DoubleProperty gridWidthProperty = new SimpleDoubleProperty(SCENE_MIN_WIDTH);

    public BomberView(Stage primaryStage) {
        start(primaryStage);
    }

    private void start(Stage stage) {
        // mainConfig && bindings
        configMainComponents(stage);


        // scene & display
        this.scene = new Scene(this, SCENE_MIN_WIDTH, SCENE_MIN_HEIGHT);
        configKeyPressBinding(scene);
        stage.setScene(scene);
        stage.show();
        stage.setMinHeight(stage.getHeight());
        stage.setMinWidth(stage.getWidth());

        startButton.setOnAction(e -> {
            bomberViewModel.triggerStart();
            removeMenu();
            configMenu();
            removeGrid();
            createGrid();
        });
        configKeyPressBinding(scene);
    }

    private void configMainComponents(Stage stage) {
        stage.titleProperty().bind(bomberViewModel.titleProperty());
        startButton.textProperty().bind(startLabelProperty());
        startButton.disableProperty().bind(bomberViewModel.isGameStartableProperty().not());

        btnContainer.setPadding(new Insets(0, PADDING, PADDING + PADDING, PADDING));
        btnContainer.setCenter(startButton);
        configMenu();
        createGrid();
    }

    private void configMenu() {
        menuView = new MenuView(bomberViewModel.getMenuViewModel(), gridWidthProperty);
        menuView.minWidthProperty().set(GRID_WIDTH * 2);
        setTop(menuView);
        setBottom(btnContainer);
    }

    private void createGrid() {
        gridView = new GridView(bomberViewModel.getGridViewModel(), gridWidthProperty);

        gridView.minHeightProperty().bind(gridWidthProperty);
        gridView.minWidthProperty().bind(gridWidthProperty);
        gridView.maxHeightProperty().bind(gridWidthProperty);
        gridView.maxWidthProperty().bind(gridWidthProperty);

        setCenter(gridView);
    }

    private void removeGrid() {
        this.getChildren().remove(gridView);
        gridView  = null;
    }

    private void removeMenu() {
        this.getChildren().remove(menuView);
        menuView  = null;
    }

    public void configKeyPressBinding(Scene scene){
        scene.setOnKeyPressed(e -> {
            if(bomberViewModel.isGameStartedProperty().getValue()){
                GameFacade.movementProperty.setValue(Movement.NULL);
                GameFacade.movementProperty.setValue(Movement.getKeyCode(e.getCode()));
            }
        });
    }


    public ReadOnlyStringProperty startLabelProperty() { return new SimpleStringProperty(BTN_PLAY); }
}
