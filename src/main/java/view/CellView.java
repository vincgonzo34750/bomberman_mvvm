package view;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import model.CellValue;
import vm.CellViewModel;

public class CellView extends StackPane {

    private final ImageView imageView;

    public CellView(CellViewModel cellViewModel, DoubleBinding cellWidthProperty) {

        ReadOnlyObjectProperty<CellValue> valueProp = cellViewModel.getCellValueProperty();
        //
        imageView = new ImageView(imageParser(valueProp.getValue()));
        imageView.setPreserveRatio(true);
        imageView.fitWidthProperty().bind(cellWidthProperty);

        getChildren().add(imageView);
        valueProp.addListener((obs, old, nv )->
                setImage(imageView, nv));
    }

    private void setImage(ImageView imgView, CellValue cellValue){
        imgView.setImage(imageParser(cellValue));
    }

    private Image imageParser(CellValue cellValue) {
        switch(cellValue){
            case PILLAR:
                return new Image("pillar.jpg");
            case BOMB:
                return new Image("bombe.gif");
            case PLAYER:
                return new Image("bomberman.jpg");
            case KEY:
                return new Image("key.jpg");
            case WALL:
                return new Image("wall.jpg");
            case BONUSBOMB:
                return new Image("bonusbomb.jpg");
            case BONUSLIFE:
                return new Image("bonuslife.jpg");
            case MINE:
                return new Image("mine.gif");
            case TREASURE:
                return new Image("treasure.jpg");
            default:
                return new Image("empty.jpg");
        }
    }

}