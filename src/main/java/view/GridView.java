package view;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.DoubleProperty;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import model.GameFacade;
import model.Movement;
import vm.GridViewModel;

import static view.BomberView.GRID_WIDTH;
import static view.BomberView.PADDING;

public class GridView extends GridPane {

    public GridView(GridViewModel gridViewModel, DoubleProperty gridWidthProperty) { //TODO bonne chance là aussi
        setGridLinesVisible(false);
        setPadding(new Insets(PADDING));
        DoubleBinding cellWidthProperty = gridWidthProperty.subtract(PADDING * 2).divide(GRID_WIDTH);

        // Cellules de même taille
        RowConstraints rowConstraints = new RowConstraints();
        rowConstraints.setPercentHeight(100.0 / GRID_WIDTH);
        ColumnConstraints columnConstraints = new ColumnConstraints();
        columnConstraints.setPercentWidth(100.0 / GRID_WIDTH);
        for (int i = 0; i < GRID_WIDTH; ++i) {
            getColumnConstraints().add(columnConstraints);
            getRowConstraints().add(rowConstraints);
        }

        // Remplissage de la grille
        for (int i = 0; i < GRID_WIDTH; ++i) {
            for (int j = 0; j < GRID_WIDTH; ++j) {
                CellView caseView = new CellView(gridViewModel.getCellViewModel(i, j), cellWidthProperty);
                add(caseView, j, i); // lignes/colonnes inversées dans gridpane
            }
        }

    }
}
